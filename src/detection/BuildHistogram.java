package detection;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import org.apache.commons.math.MathException;
import org.apache.commons.math.distribution.NormalDistribution;
import org.apache.commons.math.distribution.NormalDistributionImpl;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class BuildHistogram {

	abstract public static class BinFactory {
		/**
		 * Stores how many bins are for one feature. The bins have equal length.
		 */
		protected int[] binCounts;
		/**
		 * For efficiency reasons, the bins are stored an retrieved from a pool.
		 */
		protected Bin[] binPool;

		public BinFactory(Settings settings) {
			super();
			binCounts = new int[settings.getFeaturesToSelectCount()];
			for (int i = 0; i < binCounts.length; i++)
				binCounts[i] = settings.getBinCount(i);
			binPool = new Bin[settings.getFeaturesToSelectCount()];
			for (int i = 0; i < binPool.length; i++)
				binPool[i] = new Bin(settings);
		}

		public void setBinCount(int feature, int count) {
			if (count <= 0 || count > 1000)
				throw new RuntimeException(
						"Bincount must be beetwen 1 and 1000.");
			binCounts[feature] = count;
		}

		/**
		 * Map the given value to a bin. The bin is retrieved from a bin pool,
		 * so you must first write it out to the Mapper.Context object before
		 * mappping an another value for the same feature!
		 * 
		 * @param feature
		 * @param value
		 * @return
		 */
		abstract public Bin mapToBin(int feature, double value);
	}

	public static class MinMaxNormedBinFactory extends BinFactory {
		public MinMaxNormedBinFactory(Settings settings) {
			super(settings);
		}

		@Override
		public Bin mapToBin(int feature, double value) {
			Bin bin = binPool[feature];
			bin.featureIndex = feature;
			if (1.0 - value > Double.MIN_VALUE)
				bin.binIndex = binCounts[feature] - 1;
			bin.binIndex = (int) Math.floor(value * binCounts[feature]);
			return bin;
		}
	}

	public static class ZNormedBinFactory extends BinFactory {
		private NormalDistribution distribution;

		public ZNormedBinFactory(Settings settings) {
			super(settings);
			distribution = new NormalDistributionImpl();
		}

		private double distributionFunction(double x) {
			try {
				return distribution.cumulativeProbability(x);
			} catch (MathException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public Bin mapToBin(int feature, double value) {
			double mappedTo_0_1 = distributionFunction(value);
			Bin bin = binPool[feature];
			bin.featureIndex = feature;
			if (1.0 - mappedTo_0_1 > Double.MIN_VALUE)
				bin.binIndex = binCounts[feature] - 1;
			bin.binIndex = (int) Math.floor(mappedTo_0_1 * binCounts[feature]);
			return bin;
		}
	}

	public static class Bin implements WritableComparable<Bin>, Serializable {
		private static final long serialVersionUID = 5658342846797069645L;
		private int featuresNum;
		public int featureIndex;
		public int binIndex;

		public Bin() {
			super();
			featureIndex = -1;
			binIndex = -1;
			featuresNum = -1;
		}

		public Bin(Settings settings) {
			this();
			featuresNum = settings.getFeaturesToSelectCount();
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			featureIndex = in.readInt();
			binIndex = in.readInt();
			featuresNum = in.readInt();
		}

		@Override
		public void write(DataOutput out) throws IOException {
			out.writeInt(featureIndex);
			out.writeInt(binIndex);
			out.writeInt(featuresNum);
		}

		@Override
		public int compareTo(Bin another) {
			return hashCode() - another.hashCode();
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Bin))
				return false;
			Bin another = (Bin) o;
			return compareTo(another) == 0;
		}

		@Override
		public int hashCode() {
			return binIndex * featuresNum + featureIndex;
		}

		@Override
		public String toString() {
			return "Bin feature-index=" + featureIndex + " ,bin-index="
					+ binIndex;
		}

		public Bin copy() {
			Bin bin = new Bin();
			bin.featureIndex = featureIndex;
			bin.binIndex = binIndex;
			bin.featuresNum = featuresNum;
			return bin;
		}
	}

	public static class Map extends
			Mapper<LongWritable, FilterInput.Record, Bin, LongWritable> {
		private LongWritable one = new LongWritable(1);
		private BinFactory binFactory;
		private Settings settings;

		@Override 
		protected void setup(Context context) {
			settings = new Settings(context.getConfiguration());
			if (settings.getNormalizationType() == Settings.NormalizationType.MINMAX)
				binFactory = new MinMaxNormedBinFactory(settings);
			else
				binFactory = new ZNormedBinFactory(settings);
		}

		@Override
		protected void map(LongWritable key, FilterInput.Record value,
				Context context) throws IOException, InterruptedException {
			for (int i = 0; i < value.asVector().getDimension(); i++)
				context.write(binFactory.mapToBin(i, value.asVector().getEntry(i)),one);
		}
	}

	public static class Combine extends
			Reducer<Bin, LongWritable, Bin, LongWritable> {
		private LongWritable countW = new LongWritable();

		@Override
		protected void reduce(Bin key, Iterable<LongWritable> values,
				Context context) throws IOException, InterruptedException {
			long count = 0;
			for (@SuppressWarnings("unused")
			LongWritable value : values)
				count++;
			countW.set(count);

			context.write(key, countW);
		}
	}

	public static class Reduce extends
			Reducer<Bin, LongWritable, NullWritable, NullWritable> {
		private java.util.Map<Bin, Long> map = new HashMap<Bin, Long>();
		private java.util.HashMap<Bin, Double> normalizedMap = new HashMap<Bin, Double>();
		private java.util.Map<Integer, Long> maximumSizes;
		private Settings settings;

		@Override
		protected void setup(Context context) {
			settings = new Settings(context.getConfiguration());

			maximumSizes = new HashMap<Integer, Long>();
			for (int i = 0; i < settings.getFeaturesToSelectCount(); i++)
				maximumSizes.put(new Integer(i), new Long(0));
		}

		@Override
		protected void reduce(Bin key, Iterable<LongWritable> values,
				Context context) throws IOException, InterruptedException {
			long count = 0;
			for (LongWritable value : values)
				count = count + value.get();

			if (count > maximumSizes.get(key.featureIndex))
				maximumSizes.put(new Integer(key.featureIndex), count);

			map.put(key.copy(), count);
		}

		protected void NormalizeMap() {
			for (java.util.Map.Entry<Bin, Long> entry : map.entrySet())
				normalizedMap.put(entry.getKey(),entry.getValue() / ((double) maximumSizes.get(entry.getKey().featureIndex)));
		}

		@Override
		protected void cleanup(Context context) throws IOException {
			NormalizeMap();
			
			String uri = context.getConfiguration().get("histogram.outputfile.full");
			FileSystem fs;
			try {
				fs = FileSystem.get(new URI(uri), context.getConfiguration());
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
			ObjectOutputStream oos = new ObjectOutputStream(fs.create(new Path(
					uri)));
			try {
				oos.writeObject(normalizedMap);
			} finally {
				oos.close();
			}
		}
	}
}
