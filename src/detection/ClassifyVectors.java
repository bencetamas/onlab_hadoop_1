package detection;

import java.io.IOException;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;
import org.apache.mahout.clustering.iterator.ClusterWritable;
import org.apache.mahout.clustering.kmeans.Kluster;
import org.apache.mahout.common.distance.DistanceMeasure;
import org.apache.mahout.common.distance.EuclideanDistanceMeasure;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.VectorWritable;

public class ClassifyVectors {

	public static class AbstractMap extends Mapper<LongWritable, VectorWritable, LongWritable, Text> {
		private List<Kluster> clusters = new ArrayList<Kluster>();
		private DistanceMeasure measure;
		private double treshold;
		private long false_pos;
		private long false_neg;
		private long true_pos;
		private long true_neg;
		
		private void loadClusters(String fileName, Configuration conf) throws IOException {
			SequenceFile.Reader.Option roptPath = SequenceFile.Reader.file(new Path(fileName));
			SequenceFile.Reader reader = new SequenceFile.Reader(conf, roptPath);
			try {
				IntWritable key = new IntWritable();
				ClusterWritable value = new ClusterWritable();
				while (reader.next(key, value)) {
					if (!(value.getValue() instanceof Kluster))
						Logger.getLogger(this.getClass()).info("The readed cluster isn't a cluster from KMeans, as expected!");
					else
						clusters.add((Kluster) value.getValue());
				}
			} finally {
				reader.close();
			}
		}
		
		@Override 
		protected void setup(Context context) throws IOException {
			// Loading clusters
			String uri = context.getConfiguration().get("mahout.input.dir");
			FileSystem fs;
			try {
				fs = FileSystem.get(new java.net.URI(uri),context.getConfiguration());
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
			FileStatus[] files = fs.listStatus(new Path(uri));
			
			for (int i = 0; i < files.length ; i++)
				if (files[i].getPath().getName().startsWith("part-r-"))
					loadClusters(files[i].getPath().toString(), context.getConfiguration());
			System.out.println("Data classification: found "+clusters.size()+" clusters.");
			Logger.getLogger(this.getClass()).info("Found "+clusters.size()+" clusters.");
			
			// Get the distance measure
			if (context.getConfiguration().get("mahout.measure").toUpperCase().equals("EUCLIDEANDISTANCEMEASURE"))
				measure = new EuclideanDistanceMeasure();
			else
				throw new RuntimeException("Wrong distance measure!");
			
			// Get the treshold
			treshold = context.getConfiguration().getDouble("mahout.treshold", 0.0);
			
			// Init counters
			false_pos = 0;
			false_neg = 0;
			true_pos = 0;
			true_neg = 0;
		}
		
		/**
		 * Writes to the context object, empty method, child-classes can override. Called from map mehtod.
		 * @param context
		 * @param key
		 * @param value
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		protected void writeMapToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			
		}
		
		/**
		 * Writes to the context object, empty method, child-classes can override. Called from cleanup mehtod.
		 * @param context
		 * @param key
		 * @param value
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		protected void writeCleanupToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			
		}
		
		protected boolean isNormal(NamedVector nv) {
			return nv.getName().endsWith("normal") || nv.getName().endsWith("neptune") || nv.getName().endsWith("smurf");
		}
		
		@Override
		protected void map(LongWritable key, VectorWritable value, Context context) throws IOException, InterruptedException {
			if (!(value.get() instanceof NamedVector)) {
				Logger.getLogger(this.getClass()).info("The readed vector isn't a namedvector, as expected!");
				return;
			}
			NamedVector nv = (NamedVector) value.get();
			
			for (int i=0 ; i < clusters.size() ; i++) {
				// TODO ????
				/*if (clusters.get(getClosestIndex()).getNumObservations() < criticalTreshold)
					this is an anomaly
				*/
				
				double pdf = 1 / (1.0+measure.distance(nv, clusters.get(i).getCenter())); 
				if (pdf > treshold) {
					// this case this is considered as normal
					if (isNormal(nv))
						true_neg++;
					else {
						false_neg++;
						writeMapToContext(context, key, new Text("Detected anomaly instance as normal, at record #"+key.get()+" clustered to  cluster "+i
								                    +" (cluster classification treshold: "+context.getConfiguration().getDouble("mahout.treshold",-1.0)+" pdf value:"+pdf
								                    +" attack type: " + nv.getName()+" raw data: " + nv.asFormatString()+")"));
					}					
					return;
				}
			}
			
			if (isNormal(nv)) {
				false_pos++;
				writeMapToContext(context, key, new Text("Detected normal instance as anomaly, at record #"+key.get()
						                    +" (cluster classification treshold: "+context.getConfiguration().getDouble("mahout.treshold",-1.0)
						                    +" attack type: " + nv.getName()+" raw data: " + nv.asFormatString()+")"));
			
			} else
				true_pos++;
		}
		
		@Override 
		protected void cleanup(Context context) throws IOException, InterruptedException {
			writeCleanupToContext(context, new LongWritable(0), new Text(""+false_pos));
			writeCleanupToContext(context, new LongWritable(1), new Text(""+false_neg));
			writeCleanupToContext(context, new LongWritable(2), new Text(""+true_pos));
			writeCleanupToContext(context, new LongWritable(3), new Text(""+true_neg));
		}
	}
	
	public static class DetailedMap extends AbstractMap {
		@Override
		protected void writeMapToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			context.write(key, value);
		}	
	}
	
	public static class SummaryMap extends AbstractMap {
		@Override
		protected void writeCleanupToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			context.write(key, value);
		}	
	}
	
	public static class SummaryReduce extends Reducer<LongWritable,Text,LongWritable,Text> {
		private long false_pos;
		private long false_neg;
		private long true_pos;
		private long true_neg;
		
		@Override
		protected void setup(Context context) {
			false_pos = 0;
			false_neg = 0;
			true_pos = 0;
			true_neg = 0;
		}
		
		@Override 
		protected void reduce(LongWritable key, Iterable<Text> values, Context context) {
			for (Text value : values) {
				if (key.get() == 0)
					false_pos += Long.parseLong(value.toString());
				else if (key.get() == 1)
					false_neg += Long.parseLong(value.toString());
				else if (key.get() == 2)
					true_pos += Long.parseLong(value.toString());
				else if (key.get() == 3)
					true_neg += Long.parseLong(value.toString());
			}
		}
		
		@Override 
		protected void cleanup(Context context) throws IOException, InterruptedException {
			long num = false_pos+false_neg+true_pos+true_neg;
			context.write(new LongWritable(-1), new Text("Total          : " + num));
			context.write(new LongWritable(-1), new Text("False positives: " + false_pos));
			context.write(new LongWritable(-1), new Text("False negatives: " + false_neg));
			context.write(new LongWritable(-1), new Text("True positives : " + true_pos));
			context.write(new LongWritable(-1), new Text("True negatives : " + true_neg));
		}
	}

}
