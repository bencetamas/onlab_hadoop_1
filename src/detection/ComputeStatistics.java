package detection;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Random;
import org.apache.commons.math.linear.Array2DRowRealMatrix;
import org.apache.commons.math.linear.ArrayRealVector;
import org.apache.commons.math.linear.LUDecompositionImpl;
import org.apache.commons.math.linear.RealMatrix;
import org.apache.commons.math.linear.RealVector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.log4j.Logger;

public class ComputeStatistics {
	
	public static class Statistics implements Serializable, Writable {
		private static final long serialVersionUID = 4502790874651865443L;
		
		private static double covEstimateInverseStepSize = 6e-3;
		private static long covEstimateInverseMaxSteps = 200;
		private static long covEstimateInveresMinSteps = 1;
		
		private RealVector sum;
		private RealVector squaredSum;
		private RealMatrix sumProduct;
		private long num;
		
		private RealVector avgCache=null;
		private RealVector devCache=null;
		private RealMatrix covCache=null;
		private RealMatrix covInverseCache=null;
		private RealVector min;
		private RealVector max;
		
		/**
		 * True if the statistics in computing phase
		 */
		private boolean cacheIsObsolote;
		
		private RealMatrix makeNonSingular(RealMatrix m, double stepSize, long maxSteps, long minSteps) {
			RealMatrix est = m.copy();
			Random rand = new Random(System.currentTimeMillis());
			int probes=0;
			while (probes < minSteps || !(new LUDecompositionImpl(est)).getSolver().isNonSingular()) {
				double delta;
				if (rand.nextBoolean())
					delta = stepSize;
				else
					delta = -stepSize;
				for (int i = 0; i < est.getColumnDimension() && i < est.getRowDimension() ; i++)
					est.addToEntry(i, i, delta);
				
				if (probes > maxSteps)
					return null;
				probes++;
			}
			return est;
		}
		
		public void RefreshCache() {
			cacheIsObsolote = false;
			
			// sum/num
			avgCache = sum.mapDivide(num);
			// sqrt(squedSum-avgCache^2)
			devCache = squaredSum.mapDivide(num).add(avgCache.mapPow(2)).mapSqrtToSelf();
			
			// (sumProduct - sum x avg - avg x sum + n * avg x avg) / (N-1)
			covCache = sumProduct.subtract(sum.outerProduct(avgCache))
								 .subtract(avgCache.outerProduct(sum))
								 .add(avgCache.mapMultiply(num).outerProduct(avgCache))
								 .scalarMultiply(1/((double) num-1));
			// produce a non-singular matrix, calc inverse
			RealMatrix covCacheEstimate;
			if (!(new LUDecompositionImpl(covCache)).getSolver().isNonSingular()) {
				Logger.getLogger(this.getClass()).warn("The covariance matrix is no singular, trying to make it singular...");
				covCacheEstimate = makeNonSingular(covCache, covEstimateInverseStepSize, covEstimateInverseMaxSteps, covEstimateInveresMinSteps);
				if (covCacheEstimate == null)
					Logger.getLogger(this.getClass()).warn("...cannit make the covariance matrix singular!");
				else
					Logger.getLogger(this.getClass()).info("Made the covariance matrix to singular.");
			} else
				covCacheEstimate = covCache;
			if (covCacheEstimate == null)
				covInverseCache = null;
			else
				covInverseCache = (new LUDecompositionImpl(covCacheEstimate)).getSolver().getInverse();
		}
		
		private void checkCache() {
			if (cacheIsObsolote)
				RefreshCache();
		}
		
		public RealVector avg() { checkCache(); return avgCache; }
		public RealVector dev() { checkCache(); return devCache; }
		public RealMatrix cov() { checkCache(); return covCache; }
		/**
		 * Returns the inverse of the covariance matrix. If it doesn't have inverse, return null.
		 * @return
		 */
		public RealMatrix covInverse() { checkCache(); return covInverseCache; }
		public RealVector min() { return min; }
		public RealVector max() { return max; }
		public long num() { return num; }
		
		public void RegisterData(RealVector data) throws RuntimeException {
			cacheIsObsolote = true;
			sum = sum.add(data);
			squaredSum = squaredSum.add(data.mapPow(2.0));
			sumProduct = sumProduct.add(data.outerProduct(data));
			
			// set min and max values
			for (int i=0; i < data.getDimension() ; i++) {
				if (data.getEntry(i) < min.getEntry(i))
					min.setEntry(i, data.getEntry(i));
				if (data.getEntry(i) > max.getEntry(i))
						max.setEntry(i, data.getEntry(i));
			}
				
			num++;
		}
		
		public void AggregateStatistics(Statistics another) {
			cacheIsObsolote = true;
			sum = sum.add(another.sum);
			squaredSum = squaredSum.add(another.squaredSum);
			sumProduct = sumProduct.add(another.sumProduct);
			
			// set min and max values
			for (int i=0; i < min.getDimension() ; i++) {
				if (another.min.getEntry(i) < min.getEntry(i))
					min.setEntry(i, another.min.getEntry(i));
				if (another.max.getEntry(i) > max.getEntry(i))
						max.setEntry(i, another.max.getEntry(i));
			}
			
			num = num + another.num;
		}
		
		public Statistics() {
			super();
			sum = null;
			squaredSum = null;
			sumProduct = null;
			min = null;
			max = null;
			num = 0;
			cacheIsObsolote = true;
		}
		
		public Statistics(int dimensions) {
			sum = new ArrayRealVector(dimensions);
			squaredSum = new ArrayRealVector(dimensions);
			sumProduct = new Array2DRowRealMatrix(dimensions,dimensions);
			min = new ArrayRealVector(dimensions);
			max = new ArrayRealVector(dimensions);
			for (int i=0; i < dimensions ; i++) {
				min.setEntry(i, Double.MAX_VALUE);
				max.setEntry(i, -Double.MAX_VALUE);
			}
			num = 0;
			cacheIsObsolote = true;
		}
		
		private String dumpVectorMessage(RealVector vector) {
			String msg = "{ ";
			for (int i =0 ; i < vector.getDimension() ; i++) {
				msg = msg + vector.getEntry(i);
				if (i == vector.getDimension()-1)
					msg = msg + " }";
				else
					msg = msg + ", ";
			}
			return msg;
		}
		
		private String dumpMatrixMessage(RealMatrix matrix) {
			String msg = "";
			for (int i=0 ; i < matrix.getRowDimension() ; i++) {
				if (i != 0)
					msg = msg + "\n";
				msg = msg + dumpVectorMessage(matrix.getRowVector(i));
			}
			return msg;	
		}
		
		/**
		 * Writes out statistics into stream, for debug purposes.
		 */
		public void dump(PrintStream out) {	
			out.println("Dimensions: " + sum.getDimension());
			out.println("Number of datapoints: " + num);
			out.println("Minimums:" + dumpVectorMessage(min()));
			out.println("Maximum: " + dumpVectorMessage(max()));
			out.println("Average: " + dumpVectorMessage(avg()));
			out.println("Deviation: " + dumpVectorMessage(dev()));
			out.println("Covariance:\n" + dumpMatrixMessage(cov()));
			if (covInverseCache != null)
				out.println("Covariance inverse:\n" + dumpMatrixMessage(covInverse()));
		}

		private RealVector readVector(DataInput in) throws IOException {
			ArrayRealVector vector = new ArrayRealVector(in.readInt());
			for (int i=0 ; i < vector.getDimension() ; i++)
				vector.setEntry(i, in.readDouble());
			return vector;
		}
		
		@Override
		public void readFields(DataInput in) throws IOException {
			sum = readVector(in);
			squaredSum = readVector(in);
			int cols = in.readInt();
			int rows = in.readInt();
			sumProduct = new Array2DRowRealMatrix(rows,cols);
			for (int i = 0; i < rows ; i++)
				sumProduct.setRowVector(i, readVector(in));
			min = readVector(in);
			max = readVector(in);
			num = in.readLong();
			cacheIsObsolote = true;
		}

		private void writeVector(DataOutput out, RealVector vector) throws IOException {
			out.writeInt(vector.getDimension());
			for (int i = 0; i < vector.getDimension() ; i++)
				out.writeDouble(vector.getEntry(i));
		}
		
		@Override
		public void write(DataOutput out) throws IOException {
			writeVector(out,sum);
			writeVector(out,squaredSum);
			
			out.writeInt(sumProduct.getColumnDimension());
			out.writeInt(sumProduct.getRowDimension());
			for (int i = 0; i < sumProduct.getRowDimension() ; i++)
				writeVector(out,sumProduct.getRowVector(i));
			writeVector(out, min());
			writeVector(out, max());
			out.writeLong(num);
		}
	}
	
	public static Statistics loadStatisticsFromCache(String fileName, Configuration conf) {
		Statistics s = null;
		DataInputStream in = null;
		try {
			in = new DataInputStream(new java.io.FileInputStream(Helper.getLocalCacheFile(fileName, conf)));
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
		try { try {
			s = new Statistics();
			s.readFields(in);
		} finally {
			in.close();
		}} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return s;
	}

	public static class MapInput extends Mapper<LongWritable, FilterInput.Record, NullWritable, Statistics> {
		private Statistics statistics;
		private Settings settings;
		@Override
		protected void setup(Context context) {
			settings = new Settings(context.getConfiguration());
			statistics = new Statistics(settings.getFeaturesToSelectCount());
		}
		
		@Override 
		protected void map(LongWritable key, FilterInput.Record value, Context context) throws IOException, InterruptedException {
			statistics.RegisterData(value.asVector());
		}
		
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			context.write(NullWritable.get(), statistics);
		}
	}
	
	protected static abstract class Reduce extends Reducer<NullWritable, Statistics, NullWritable, Statistics> {
		private Statistics statistics;
		protected Settings settings;
		
		protected abstract int getFeaturesNum();
		
		@Override
		protected void setup(Context context) {
			settings = new Settings(context.getConfiguration());
			statistics = new Statistics(getFeaturesNum());
		}
		
		@Override 
		protected void reduce(NullWritable key, Iterable<Statistics> values, Context context) throws IOException, InterruptedException {
			for (Statistics value : values)
				statistics.AggregateStatistics(value);
		}
		
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			context.write(NullWritable.get(), statistics);
			
			String uri = context.getConfiguration().get("statistics.outputfile.full");
			FileSystem fs;
			try {
				fs = FileSystem.get(new URI(uri), context.getConfiguration());
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
			FSDataOutputStream fsdos = fs.create(new Path(uri));
			try {
				statistics.write(fsdos);
			} finally {
				fsdos.close();
			}
		}
	}
	
	public static class ReduceInput extends Reduce {
		protected int getFeaturesNum() {
			return settings.getFeaturesToSelectCount();
		}
	};
	
	public static class MapProjected extends Mapper<LongWritable, FilterInput.Record, NullWritable, Statistics> {
		private Statistics statistics;
		private Statistics multivariateStatistics;
		
		@Override
		protected void setup(Context context) throws IOException {
			Configuration conf = context.getConfiguration();
			multivariateStatistics = loadStatisticsFromCache(conf.get("statistics.inputfile.relative"), conf);
		
			if (multivariateStatistics.covInverse() == null) {
				Logger.getLogger(this.getClass()).warn("Cannot invert covariance matrix, cannot compute Z-scores.");
				throw new RuntimeException("Cannot invert covariance matrix, cannot compute Z-scores.");
			}

			statistics = new Statistics(1);
		}
		
		@Override 
		protected void map(LongWritable key, FilterInput.Record value, Context context) throws IOException, InterruptedException {	
			ComputeZScores.ProjectedRecord record = ComputeZScores.ProjectedRecord.transform(value,multivariateStatistics);
			statistics.RegisterData(record.asVector());
		}
		
		@Override
		protected void cleanup(Context context) throws IOException, InterruptedException {
			context.write(NullWritable.get(), statistics);
		}
	}
	
	public static class ReduceProjected extends Reduce {
		protected  int getFeaturesNum() {
			return 1;
		}
	};
}
