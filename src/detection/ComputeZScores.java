package detection;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.commons.math.linear.ArrayRealVector;
import org.apache.commons.math.linear.RealVector;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

public class ComputeZScores {
	
	public static class ProjectedRecord {
		private RealVector value;
		private String attackType;
		
		public double asDouble() {
			return value.getEntry(0);
		}
		
		public RealVector asVector() {
			return value;
		}
		
		public String getAttackType() {
			return attackType;
		}
		
		public ProjectedRecord(double value, String attackType) {
			super();
			this.value = new ArrayRealVector(new double[] {value});
			this.attackType = attackType;
		}
		
		public static ProjectedRecord transform(FilterInput.Record record, ComputeStatistics.Statistics statistics) {
			RealVector xMinusAvgX = record.asVector().subtract(statistics.avg());
			double y2 = statistics.covInverse().
					preMultiply(xMinusAvgX).
					dotProduct(xMinusAvgX);
			double y;
			if (y2 < 0) 
				y = 0;
			else
				y = java.lang.Math.sqrt(y2);
			return new ProjectedRecord(y,record.getAttackType());
		}
	}
	
	public static class ZScoreRecord implements Writable {
		public double score;
		public String attack_type;
		
		
		public ZScoreRecord() {
			super();
			score = -1;
			attack_type = "unknown";
		}
		
		public ZScoreRecord(double score, String attack_type) {
			super();
			this.score = score;
			this.attack_type = attack_type;
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			score = in.readDouble();
			attack_type = in.readUTF();
		}

		@Override
		public void write(DataOutput out) throws IOException {
			out.writeDouble(score);
			out.writeUTF(attack_type);
		}
	}

	public static class Map extends Mapper<LongWritable, FilterInput.Record, LongWritable, ZScoreRecord> {
		private ComputeStatistics.Statistics multivariateStatistics = null;
		private double projectedAvg;
		private double projectedDev;
		
		@Override 
		protected void setup(Context context) {
			Configuration conf = context.getConfiguration();
			multivariateStatistics = ComputeStatistics.loadStatisticsFromCache(conf.get("statistics.multivariate.relative"),conf);
			ComputeStatistics.Statistics projectedStatistics = ComputeStatistics.loadStatisticsFromCache(conf.get("statistics.projected.relative"),conf);
			projectedAvg = projectedStatistics.avg().getEntry(0);
			projectedDev = projectedStatistics.dev().getEntry(0);
			
			if (multivariateStatistics.covInverse() == null) {
				Logger.getLogger(this.getClass()).warn("Cannot invert covariance matrix, cannot compute Z-scores.");
				throw new RuntimeException("Cannot invert covariance matrix, cannot compute Z-scores.");
			}
		}
		
		@Override
		protected void map(LongWritable key, FilterInput.Record value, Context context) throws IOException, InterruptedException {
			ProjectedRecord projected = ProjectedRecord.transform(value, multivariateStatistics);
			// TODO optimization new ZScoreRecord -> ZScoreRecord.set(...)
			ZScoreRecord zScore = new ZScoreRecord(Math.abs(projected.asDouble()-projectedAvg) / projectedDev,value.getAttackType());
			context.write(key, zScore);
		}
	}
}
