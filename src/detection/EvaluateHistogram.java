package detection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

import detection.BuildHistogram.BinFactory;
import detection.BuildHistogram.MinMaxNormedBinFactory;
import detection.BuildHistogram.ZNormedBinFactory;

public class EvaluateHistogram {
	
	public static abstract class AbstractMap extends Mapper<LongWritable, FilterInput.Record, LongWritable, Text>{
		private java.util.Map<BuildHistogram.Bin, Double> map;
		private long false_pos;
		private long false_neg;
		private long true_pos;
		private long true_neg;
		
		private BinFactory binFactory;
		private Settings settings;
		
		/**
		 * Writes to the context object, empty method, child-classes can override. Called from map mehtod.
		 * @param context
		 * @param key
		 * @param value
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		protected void writeMapToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			
		}
		
		/**
		 * Writes to the context object, empty method, child-classes can override. Called from cleanup mehtod.
		 * @param context
		 * @param key
		 * @param value
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		protected void writeCleanupToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			
		}
		
		@SuppressWarnings("unchecked")
		@Override
		protected void setup(Context context) {
			settings = new Settings(context.getConfiguration());
			if (settings.getNormalizationType() == Settings.NormalizationType.MINMAX)
				binFactory = new MinMaxNormedBinFactory(settings);
			else
				binFactory = new ZNormedBinFactory(settings);
			
			map = null;
			Configuration conf = context.getConfiguration();
			String fileName = context.getConfiguration().get("histogram.relative");
			ObjectInputStream in = null;
			try { try {
				in = new ObjectInputStream(new FileInputStream(Helper.getLocalCacheFile(fileName, conf)));
				map = ((java.util.HashMap<BuildHistogram.Bin, Double>) in.readObject());
			} finally {
				in.close();
			}} catch (Exception e) {
				throw new RuntimeException(e);				
			}
			
			false_pos = 0;
			false_neg = 0;
			true_pos = 0;
			true_neg = 0;
		}
		
		private double computeHBOS(FilterInput.Record record) {
			double r = 0.0;
			for (int i = 0 ; i < record.asVector().getDimension() ; i++) {
				double featureValue = map.get(binFactory.mapToBin(i, record.asVector().getEntry(i)));			
				r = r + Math.log(1 / featureValue);
			}
			return r;
		}
		
		protected boolean isNormal(FilterInput.Record value) {
			return value.getAttackType().equals("normal") || value.getAttackType().equals("neptune") || value.getAttackType().equals("smurf");
		}
		
		@Override
		protected void map(LongWritable key, FilterInput.Record value, Context context) throws IOException, InterruptedException {
			double HBOS = computeHBOS(value);
			double boundary = context.getConfiguration().getDouble("HBOS.boundary", -6.0);
			if (HBOS < boundary && isNormal(value))
				true_neg++;
			else if (HBOS >= boundary && !isNormal(value))
				true_pos++;
			else if (HBOS < boundary && !isNormal(value)) {
				false_neg++;
				writeMapToContext(context, key, new Text("Detected anomaly instance as normal, at record #"+key.get()+" with HBOS "+HBOS
	                    +" (attack type"+value.getAttackType()+" raw data"+value.asVector().toString()+")."));
			} else {
				false_pos++;
				writeMapToContext(context, key, new Text("Detected normal instance as anomaly, at record #"+key.get()+" with HBOS "+HBOS
	                    +" (attack type"+value.getAttackType()+" raw data"+value.asVector().toString()+")."));
			}
		}
		
		@Override 
		protected void cleanup(Context context) throws IOException, InterruptedException {
			writeCleanupToContext(context, new LongWritable(0), new Text(""+false_pos));
			writeCleanupToContext(context, new LongWritable(1), new Text(""+false_neg));
			writeCleanupToContext(context, new LongWritable(2), new Text(""+true_pos));
			writeCleanupToContext(context, new LongWritable(3), new Text(""+true_neg));
		}
	}
	
	public static class DetailedMap extends AbstractMap {
		@Override
		protected void writeMapToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			context.write(key, value);
		}	
	}
	
	public static class SummaryMap extends AbstractMap {
		@Override
		protected void writeCleanupToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			context.write(key, value);
		}	
	}
	
	public static class SummaryReduce extends Reducer<LongWritable,Text,LongWritable,Text> {
		private long false_pos;
		private long false_neg;
		private long true_pos;
		private long true_neg;
		
		@Override
		protected void setup(Context context) {
			false_pos = 0;
			false_neg = 0;
			true_pos = 0;
			true_neg = 0;
		}
		
		@Override 
		protected void reduce(LongWritable key, Iterable<Text> values, Context context) {
			for (Text value : values) {
				if (key.get() == 0)
					false_pos += Long.parseLong(value.toString());
				else if (key.get() == 1)
					false_neg += Long.parseLong(value.toString());
				else if (key.get() == 2)
					true_pos += Long.parseLong(value.toString());
				else if (key.get() == 3)
					true_neg += Long.parseLong(value.toString());
			}
		}
		
		@Override 
		protected void cleanup(Context context) throws IOException, InterruptedException {
			long num = false_pos+false_neg+true_pos+true_neg;
			context.write(new LongWritable(-1), new Text("Total          : " + num));
			context.write(new LongWritable(-1), new Text("False positives: " + false_pos));
			context.write(new LongWritable(-1), new Text("False negatives: " + false_neg));
			context.write(new LongWritable(-1), new Text("True positives : " + true_pos));
			context.write(new LongWritable(-1), new Text("True negatives : " + true_neg));
		}
	}
}
