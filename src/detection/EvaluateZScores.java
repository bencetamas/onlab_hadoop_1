package detection;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class EvaluateZScores {
	// TODO optimalization (for example: SummaryReduce cast long from String -> new Writable object to pass)
	protected static abstract class AbstractMap extends Mapper<LongWritable, ComputeZScores.ZScoreRecord, LongWritable, Text>{
		// TODO getTreshold method generalization (for all significance level and value of freedom.
		/**
		 * Returns the treshold of t-distribution at significance level sign_level, value of freedom value_of_freedom
		 * @param sign_level
		 * @param value_of_freedom
		 * @return
		 */
		private double getTreshold(double sign_level, long value_of_freedom) {
	//		if (value_of_freedom < 500)
	//			throw new RuntimeException("Critical value calculation for < 500 value of freedom t-distributins not supported!");
			if (sign_level >= 0.9987 && sign_level <= 0.9993) return 3.2905;
			if (sign_level >= 0.987 && sign_level <= 0.993) return 2.5758;
			if (sign_level >= 0.975 && sign_level <= 0.983) return 2.3263;
			if (sign_level >= 0.965 && sign_level <= 0.975) return 2.1704;
			if (sign_level >= 0.955 && sign_level <= 0.965) return 2.0540;
			if (sign_level >= 0.945 && sign_level <= 0.955) return 1.9600;
			if (sign_level >= 0.935 && sign_level <= 0.945) return 1.8810;
			if (sign_level >= 0.925 && sign_level <= 0.935) return 1.8121;
			if (sign_level >= 0.915 && sign_level <= 0.925) return 1.7509;
			if (sign_level >= 0.905 && sign_level <= 0.915) return 1.6955;
			if (sign_level >= 0.895 && sign_level <= 0.905) return 1.6449;
			if (sign_level >= 0.885 && sign_level <= 0.895) return 1.5983;
			if (sign_level >= 0.875 && sign_level <= 0.885) return 1.5549;
			if (sign_level >= 0.865 && sign_level <= 0.875) return 1.5142;
			if (sign_level >= 0.855 && sign_level <= 0.865) return 1.4759;
			if (sign_level >= 0.797 && sign_level <= 0.803) return 1.2816;
			
			throw new RuntimeException("Critical value calculation (t-distribution) for significance level " + sign_level + " not supported!");
		}
		
		private double crit_val;
		private ComputeStatistics.Statistics statistics;
		private long false_pos;
		private long false_neg;
		private long true_pos;
		private long true_neg;
		
		@Override
		protected void setup(Context context) {
			Configuration conf = context.getConfiguration();
			statistics = ComputeStatistics.loadStatisticsFromCache(conf.get("statistics.projected.relative"),conf);
			
			double tres =  getTreshold(context.getConfiguration().getDouble("grubbs.significance.level",0.95), statistics.num());
			crit_val = ((double) statistics.num()-1)/Math.sqrt(statistics.num()) * Math.sqrt(tres*tres/(statistics.num()-2+tres*tres));
			
			false_pos = 0;
			false_neg = 0;
			true_pos = 0;
			true_neg = 0;
		}
		
		/**
		 * Writes to the context object, empty method, child-classes can override. Called from map mehtod.
		 * @param context
		 * @param key
		 * @param value
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		protected void writeMapToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			
		}
		
		/**
		 * Writes to the context object, empty method, child-classes can override. Called from cleanup mehtod.
		 * @param context
		 * @param key
		 * @param value
		 * @throws InterruptedException 
		 * @throws IOException 
		 */
		protected void writeCleanupToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			
		}
		
		protected boolean isNormal(ComputeZScores.ZScoreRecord value) {
			return value.attack_type.equals("normal") || value.attack_type.equals("neptune") || value.attack_type.equals("smurf");
		}
		
		@Override
		protected void map(LongWritable key, ComputeZScores.ZScoreRecord value, Context context) throws IOException, InterruptedException {
			if (value.score > crit_val && !isNormal(value))
				true_pos++;
			else if (value.score > crit_val && isNormal(value)) {
				false_pos++;
				writeMapToContext(context, key, new Text("Detected normal instance as anomaly, at record #"+key.get()+" with z-score "+value.score
						                    +" (significance level: "+context.getConfiguration().getDouble("grubbs.significance.level",0.95)+" critical-value:"+crit_val
											+" attack type: " + value.attack_type));
			} else if (value.score <= crit_val && isNormal(value))
				true_neg++;
			else  {
				false_neg++;
				writeMapToContext(context, key, new Text("Detected anomaly instance as normal, at record #"+key.get()+" with z-score "+value.score
						                    +" (significance level: "+context.getConfiguration().getDouble("grubbs.significance.level",0.95)+" critical-value:"+crit_val
						                    +" attack type: " + value.attack_type));
			}
		}
		
		@Override 
		protected void cleanup(Context context) throws IOException, InterruptedException {
			writeCleanupToContext(context, new LongWritable(0), new Text(""+false_pos));
			writeCleanupToContext(context, new LongWritable(1), new Text(""+false_neg));
			writeCleanupToContext(context, new LongWritable(2), new Text(""+true_pos));
			writeCleanupToContext(context, new LongWritable(3), new Text(""+true_neg));
		}
	}
	
	public static class DetailedMap extends AbstractMap {
		@Override
		protected void writeMapToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			context.write(key, value);
		}	
	}
	
	public static class SummaryMap extends AbstractMap {
		@Override
		protected void writeCleanupToContext(Context context,LongWritable key, Text value) throws IOException, InterruptedException {
			context.write(key, value);
		}	
	}
	
	public static class SummaryReduce extends Reducer<LongWritable,Text,LongWritable,Text> {
		private long false_pos;
		private long false_neg;
		private long true_pos;
		private long true_neg;
		
		@Override
		protected void setup(Context context) {
			false_pos = 0;
			false_neg = 0;
			true_pos = 0;
			true_neg = 0;
		}
		
		@Override 
		protected void reduce(LongWritable key, Iterable<Text> values, Context context) {
			for (Text value : values) {
				if (key.get() == 0)
					false_pos += Long.parseLong(value.toString());
				else if (key.get() == 1)
					false_neg += Long.parseLong(value.toString());
				else if (key.get() == 2)
					true_pos += Long.parseLong(value.toString());
				else if (key.get() == 3)
					true_neg += Long.parseLong(value.toString());
			}
		}
		
		@Override 
		protected void cleanup(Context context) throws IOException, InterruptedException {
			long num = false_pos+false_neg+true_pos+true_neg;
			context.write(new LongWritable(-1), new Text("Total          : " + num));
			context.write(new LongWritable(-1), new Text("False positives: " + false_pos));
			context.write(new LongWritable(-1), new Text("False negatives: " + false_neg));
			context.write(new LongWritable(-1), new Text("True positives : " + true_pos));
			context.write(new LongWritable(-1), new Text("True negatives : " + true_neg));
		}
	}
}
