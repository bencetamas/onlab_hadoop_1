package detection;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.VectorWritable;

public class ExportToMahout {
	
	public static class Map extends Mapper<LongWritable, FilterInput.Record, LongWritable, VectorWritable> {
		private DenseVector dense;
		private VectorWritable writable = new VectorWritable();
		private Settings settings;
		
		@Override
		protected void setup(Context context) {
			settings = new Settings(context.getConfiguration());
			dense = new DenseVector(settings.getFeaturesToSelectCount());
		}
		
		@Override
		protected void map(LongWritable key, FilterInput.Record value, Context context) throws IOException, InterruptedException {
			dense.assign(value.asVector().getData());
			String additionalInfo = key.toString() + "\t" + value.getAttackType();
			NamedVector named = new NamedVector(dense,additionalInfo);
			writable.set(named);
			context.write(key, writable);
		}
	}
}
