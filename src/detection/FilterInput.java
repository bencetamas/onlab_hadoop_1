package detection;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.commons.math.linear.ArrayRealVector;
import org.apache.commons.math.linear.RealVector;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;

public class FilterInput {
	
	/**
	 * The Record type will be used in the further analysis. Row TCP data is converted into this type.
	 * @author tamasbence
	 *
	 */
	public static class Record implements Writable {
		private RealVector vector;
		private String attackType;
		
		public RealVector asVector() {
			return vector;
		}
		
		public void setAsVector(RealVector vector) {
			this.vector = vector;
		}
		
		public String getAttackType() {
			return attackType;
		}
		
		public Record() {
			super();
			attackType = null;
			vector = null;
		}
		
		public Record(double [] raw_vector, String attackType) {
			super();
			this.attackType = attackType;
			vector = new ArrayRealVector(raw_vector);
		}

		@Override
		public void readFields(DataInput in) throws IOException {
			attackType = in.readUTF();
			int dimCount = in.readInt();
			vector = new ArrayRealVector(dimCount);
			for (int i = 0 ; i < dimCount ; i++)
				vector.setEntry(i, in.readDouble());
		}

		@Override
		public void write(DataOutput out) throws IOException {
			out.writeUTF(attackType);
			out.writeInt(vector.getDimension());
			for (int i = 0 ; i < vector.getDimension() ; i++)
				out.writeDouble(vector.getEntry(i));
		}
	}

	public static class Map extends Mapper<LongWritable, Text, LongWritable, Record> {
		private Settings settings;
		
		@Override
		public void setup(Context context) {
			settings = new Settings(context.getConfiguration());
		}
		
		@Override 
		public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
			try {
				String [] line = value.toString().split(",");
				String attackType = line[settings.getAttackTypeIndex()].toLowerCase();
				if (attackType.endsWith("."))
					attackType = attackType.substring(0, attackType.length()-1);
				
				double [] raw = new double [settings.getFeaturesToSelectCount()];
				
				for (int i = 0 ; i < settings.getFeaturesToSelectCount() ; i++)
					raw[i] = Double.parseDouble(line[settings.getFeaturesToSelect(i)]);
			
				Record filtered = new Record(raw,attackType);
				context.write(key, filtered);	
			} catch (Exception e) {
				Logger.getLogger(this.getClass()).warn("Error when processing input record, record dropped.");
				Logger.getLogger(this.getClass()).warn("        Record raw content: '" + value.toString() + "'");
				Logger.getLogger(this.getClass()).debug("Error message",e);
			}
		}
	}
}
