package detection;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

public class Helper {

	@SuppressWarnings("deprecation")
	public static String getLocalCacheFile(String fileName , Configuration conf) {
		Path[] cachedFiles;
		try {
			cachedFiles = DistributedCache.getLocalCacheFiles(conf);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		if (cachedFiles == null) {
			Logger.getLogger("detection.Helper.getLocalCacheFile").warn("No cache files found! Cannot load cache file!");
			throw new RuntimeException("No cache files found! Cannot load cache file!");
		} else {
			for (int i = 0 ; i < cachedFiles.length ; i++) 
				if (cachedFiles[i].toString().endsWith(fileName))
					return cachedFiles[i].toString();
			Logger.getLogger("ComputeStatistics.loadStatisticsFromCache").warn("Unable to load cache file!");
			throw new RuntimeException("Unable to load cache file!");
		}
	}
}
