package detection;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.mahout.clustering.canopy.CanopyDriver;
import org.apache.mahout.clustering.kmeans.KMeansDriver;
import org.apache.mahout.common.distance.EuclideanDistanceMeasure;
import org.apache.mahout.math.VectorWritable;

public class JobController {
	public static String multivariateStatisticsDir = "/multivariate_statistics/";
	public static String multivariateStatisitcsFileName = "multivariate.stat";
	public static String transformedStatisticsDir = "/transformed_statistics/";
	public static String transformedStatisitcsFileName = "transformed.stat";
	public static String rawDataPath = "/rawdata/";
	public static String filteredDataPath = "/filtered/";
	public static String zScoresDataPath = "/zscores/";
	public static String grubbsTestResultPath = "/grubbs/";
	public static String grubbsTestResultSummaryPath = "/grubbsSummary/";	
	public static String zNormalizedDataPath = "/znormalized/";
	public static String minMaxNormalizedDataPath = "/minmaxnormalized/";
	public static String normalizedCSVPath = "/normalizedCSV/";
	public static String mahoutInputPointsPath = "/mahoutpoints/";
	public static String canopyOutputPath = "/canopyclusters/";
	public static String kmeansInputClusters = canopyOutputPath + "clusters-0-final/";
	public static String kmeansOutputPath = "/kmeansclusters/";
	public static String kmeansClusteredPointsPath = kmeansOutputPath + "clusteredPoints/";
	public static String mahoutDecodedOutputPath = "/mahoutdecoded/";
	public static String mahoutClusteringPath = "/mahoutclustering/";
	public static String mahoutClusteringSummaryPath = "/mahoutsummary/";
	public static String histogramFileName = "histogram.map";
	public static String histogramDir = "/histogram/";
	public static String HBOSDataPath = "/HBOS/";
	public static String HBOSSummaryPath = "/HBOSSummary/";
	
	public static String normalizedDataPath() {
		if (getCurrentSettings().getNormalizationType() == Settings.NormalizationType.ZNORM)
			return zNormalizedDataPath;
		else
			return minMaxNormalizedDataPath;
	}
	
	public static class FilterInputJob extends Configured implements Tool {

		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection:filter input");
		 	job.setJarByClass(detection.JobController.FilterInputJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(FilterInput.Record.class);
		 	job.setOutputFormatClass(SequenceFileOutputFormat.class);
		 	job.setInputFormatClass(TextInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+rawDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+filteredDataPath));
		 	
		 	job.setMapperClass(FilterInput.Map.class);
		 	job.setNumReduceTasks(0);
		 	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class MultivariateStatisticsJob extends Configured implements Tool {
		// TODO filterinputJOb and MultivariateStitisticsJob chaining
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.outputfile.full", getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName);
			conf.set("statistics.outputfile.relative", multivariateStatisitcsFileName);
			conf.set("statistics.outputfile.dir", getCurrentSettings().getHDFSPath()+multivariateStatisticsDir);
			
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: make (multivariate) statistics");
		 	job.setJarByClass(detection.JobController.MultivariateStatisticsJob.class);
		 	
		 	job.setOutputKeyClass(NullWritable.class);
		 	
		 	job.setOutputValueClass(ComputeStatistics.Statistics.class);
		 	job.setOutputFormatClass(NullOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+filteredDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path("dummy path"));
		 	
		 	job.setMapperClass(ComputeStatistics.MapInput.class);
		 	job.setReducerClass(ComputeStatistics.ReduceInput.class);
		 	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class ProjectedStatisticsJob  extends Configured implements Tool {

		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.inputfile.full", getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName);
			conf.set("statistics.inputfile.relative", multivariateStatisitcsFileName);
			conf.set("statistics.outputfile.full", getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName);
			conf.set("statistics.outputfile.relative", transformedStatisitcsFileName);
			conf.set("statistics.outputfile.dir", getCurrentSettings().getHDFSPath()+transformedStatisticsDir);
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName),conf);
			Job job = Job.getInstance(conf);
	
		 	job.setJobName("Anomaly detection: make statistics on transformed data");
		 	job.setJarByClass(detection.JobController.ProjectedStatisticsJob.class);
		 	
		 	job.setOutputKeyClass(NullWritable.class);
		 	job.setOutputValueClass(ComputeStatistics.Statistics.class);
		 	job.setOutputFormatClass(NullOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+filteredDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path("dummy path"));
		 	
		 	job.setMapperClass(ComputeStatistics.MapProjected.class);
		 	job.setReducerClass(ComputeStatistics.ReduceProjected.class);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class ZScoreComputeJob  extends Configured implements Tool {

		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.multivariate.full", getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName);
			conf.set("statistics.multivariate.relative", multivariateStatisitcsFileName);
			conf.set("statistics.projected.full", getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName);
			conf.set("statistics.projected.relative", transformedStatisitcsFileName);
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName),conf);
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: compute z-score for Grubb's test");
		 	job.setJarByClass(detection.JobController.ZScoreComputeJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(ComputeZScores.ZScoreRecord.class);
		 	job.setOutputFormatClass(SequenceFileOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+filteredDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+zScoresDataPath));
		 	
		 	job.setMapperClass(ComputeZScores.Map.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class ZScoreEvaluateJob  extends Configured implements Tool {
		// TODO Compute and Evaluetate ZScore chaining
		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.projected.full", getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName);
			conf.set("statistics.projected.relative", transformedStatisitcsFileName);
			conf.setDouble("grubbs.significance.level", getCurrentSettings().getSignificanceLevel());
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: evaluate z-score for Grubb's test");
		 	job.setJarByClass(detection.JobController.ZScoreEvaluateJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(Text.class);
		 	
		 	job.setOutputFormatClass(TextOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+zScoresDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+grubbsTestResultPath));
		 	
		 	job.setMapperClass(EvaluateZScores.DetailedMap.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class ZScoreSummarizeJob  extends Configured implements Tool {
		// TODO Compute and Evaluetate ZScore chaining
		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.projected.full", getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName);
			conf.set("statistics.projected.relative", transformedStatisitcsFileName);
			conf.setDouble("grubbs.significance.level", getCurrentSettings().getSignificanceLevel());
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+transformedStatisticsDir+transformedStatisitcsFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: evaluate z-score for Grubb's test (summary)");
		 	job.setJarByClass(detection.JobController.ZScoreEvaluateJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(Text.class);
		 	
		 	job.setOutputFormatClass(TextOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+zScoresDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+grubbsTestResultSummaryPath));
		 	
		 	job.setMapperClass(EvaluateZScores.SummaryMap.class);
		 	job.setReducerClass(EvaluateZScores.SummaryReduce.class);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class ZNormalizeJob  extends Configured implements Tool {
		// TODO one job for znorm and minmax-norm, multiple outputs
		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.multivariate.full", getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName);
			conf.set("statistics.multivariate.relative", multivariateStatisitcsFileName);
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: normalize data - znorm");
		 	job.setJarByClass(detection.JobController.ZNormalizeJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(FilterInput.Record.class);
		 	
		 	job.setOutputFormatClass(SequenceFileOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+filteredDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+zNormalizedDataPath));
		 	
		 	job.setMapperClass(Normalize.MapZ.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class MinMaxNormalizeJob  extends Configured implements Tool {
		// TODO one job for znorm and minmax-norm, multiple outputs
		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("statistics.multivariate.full", getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName);
			conf.set("statistics.multivariate.relative", multivariateStatisitcsFileName);
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+multivariateStatisticsDir+multivariateStatisitcsFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: normalize data - 'minmax-norm'");
		 	job.setJarByClass(detection.JobController.MinMaxNormalizeJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(FilterInput.Record.class);
		 	
		 	job.setOutputFormatClass(SequenceFileOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+filteredDataPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+minMaxNormalizedDataPath));
		 	
		 	job.setMapperClass(Normalize.MapMinMax.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class MahoutExportJob  extends Configured implements Tool {
		// TODO one job for znorm and minmax-norm, multiple outputs
		@Override
		public int run(String[] arg0) throws Exception {			
			Job job = Job.getInstance(getConf());
		 	
		 	job.setJobName("Anomaly detection: export for Mahout");
		 	job.setJarByClass(detection.JobController.MahoutExportJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(VectorWritable.class);
		 	
		 	job.setOutputFormatClass(SequenceFileOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+normalizedDataPath()));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+mahoutInputPointsPath));
		 	
		 	job.setMapperClass(ExportToMahout.Map.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class BuildHistogramJob  extends Configured implements Tool {
		// TODO one job for znorm and minmax-norm, multiple outputs
		@Override
		public int run(String[] arg0) throws Exception {	
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.set("histogram.outputfile.relative", histogramFileName);
			conf.set("histogram.outputfile.dir", getCurrentSettings().getHDFSPath()+histogramDir);
			conf.set("histogram.outputfile.full", getCurrentSettings().getHDFSPath()+histogramDir+histogramFileName);
			
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: build per feature histograms");
		 	job.setJarByClass(detection.JobController.BuildHistogramJob.class);
		 	
		 	job.setOutputKeyClass(BuildHistogram.Bin.class);
		 	job.setOutputValueClass(LongWritable.class);
		 	
		 	job.setOutputFormatClass(NullOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+normalizedDataPath()));
		 	FileOutputFormat.setOutputPath(job, new Path("dummy path"));
		 	
		 	job.setMapperClass(BuildHistogram.Map.class);
		 	job.setCombinerClass(BuildHistogram.Combine.class);
		 	job.setReducerClass(BuildHistogram.Reduce.class);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class HistrogramEvaluateJob  extends Configured implements Tool {
		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.setDouble("HBOS.boundary", getCurrentSettings().getHBOSBoundary());
			conf.set("histogram.relative", histogramFileName);
			conf.set("histogram.dir", getCurrentSettings().getHDFSPath()+histogramDir);
			conf.set("histogram.full", getCurrentSettings().getHDFSPath()+histogramDir+histogramFileName);
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+histogramDir+histogramFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: evaluate with histograms");
		 	job.setJarByClass(detection.JobController.HistrogramEvaluateJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(Text.class);
		 	
		 	job.setOutputFormatClass(TextOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+normalizedDataPath()));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+HBOSDataPath));
		 	
		 	job.setMapperClass(EvaluateHistogram.DetailedMap.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class HistrogramSummarizeJob  extends Configured implements Tool {
		@SuppressWarnings("deprecation")
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = new Configuration();
			Settings.saveCurrent(conf);
			conf.setDouble("HBOS.boundary", getCurrentSettings().getHBOSBoundary());
			conf.set("histogram.relative", histogramFileName);
			conf.set("histogram.dir", getCurrentSettings().getHDFSPath()+histogramDir);
			conf.set("histogram.full", getCurrentSettings().getHDFSPath()+histogramDir+histogramFileName);
			
			DistributedCache.addCacheFile(new java.net.URI(getCurrentSettings().getHDFSPath()+histogramDir+histogramFileName),conf);
			Job job = Job.getInstance(conf);
		 	
		 	job.setJobName("Anomaly detection: evaluate with histograms (summary)");
		 	job.setJarByClass(detection.JobController.HistrogramEvaluateJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(Text.class);
		 	
		 	job.setOutputFormatClass(TextOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+normalizedDataPath()));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+HBOSSummaryPath));
		 	
		 	job.setMapperClass(EvaluateHistogram.SummaryMap.class);
		 	job.setReducerClass(EvaluateHistogram.SummaryReduce.class);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	private static String getFinalClustersPath(String dir, Configuration conf) throws IOException {
		FileSystem fs;
		try {
			fs = FileSystem.get(new java.net.URI(dir),conf);
		} catch (URISyntaxException e) {
			throw new RuntimeException(e);
		}
		FileStatus[] files = fs.listStatus(new Path(dir));
		
		for (int i = 0; i < files.length ; i++)
			if (files[i].getPath().toString().endsWith("-final"))
				return files[i].getPath().toString();
		return null;
	}
	
	public static class MahoutEvaluateJob  extends Configured implements Tool {
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = getConf();
			String clustersPath = getFinalClustersPath(getCurrentSettings().getHDFSPath()+kmeansOutputPath,conf);
			Settings.saveCurrent(conf);
			
			conf.set("mahout.input.dir",clustersPath);
			conf.setDouble("mahout.treshold", getCurrentSettings().getKmeansPdf());
			conf.set("mahout.measure", "EuclideanDistanceMeasure");

			Job job = Job.getInstance(conf);

		 	job.setJobName("Anomaly detection: evaluate mahout clustering");
		 	job.setJarByClass(detection.JobController.MahoutEvaluateJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(Text.class);
		 	
		 	job.setOutputFormatClass(TextOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+mahoutInputPointsPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+mahoutClusteringPath));
		 	
		 	job.setMapperClass(ClassifyVectors.DetailedMap.class);
		 	job.setNumReduceTasks(0);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static class MahoutSummarizeJob  extends Configured implements Tool {
		@Override
		public int run(String[] arg0) throws Exception {
			Configuration conf = getConf();
			String clustersPath = getFinalClustersPath(getCurrentSettings().getHDFSPath()+kmeansOutputPath,conf);
			Settings.saveCurrent(conf);
			
			conf.set("mahout.input.dir",clustersPath);
			conf.setDouble("mahout.treshold", getCurrentSettings().getKmeansPdf());
			conf.set("mahout.measure", "EuclideanDistanceMeasure");

			Job job = Job.getInstance(conf);

		 	job.setJobName("Anomaly detection: summarize mahout clustering");
		 	job.setJarByClass(detection.JobController.MahoutEvaluateJob.class);
		 	
		 	job.setOutputKeyClass(LongWritable.class);
		 	job.setOutputValueClass(Text.class);
		 	
		 	job.setOutputFormatClass(TextOutputFormat.class);
		 	job.setInputFormatClass(SequenceFileInputFormat.class);
		 	
		 	FileInputFormat.addInputPath(job, new Path(getCurrentSettings().getHDFSPath()+mahoutInputPointsPath));
		 	FileOutputFormat.setOutputPath(job, new Path(getCurrentSettings().getHDFSPath()+mahoutClusteringSummaryPath));
		 	
		 	job.setMapperClass(ClassifyVectors.SummaryMap.class);
		 	job.setReducerClass(ClassifyVectors.SummaryReduce.class);
	
		 	if (job.waitForCompletion(true))
		 		return 0;
		 	else
		 		return 1;
		}
		
	}
	
	public static void Debug(String [] args) {
		if ( args.length > 1 && args[0].equals("testxmlconfig")) {
			int count = Settings.getConfigCount(args[1]);
			
			for (int i = 0; i < count ; i++) {
				Settings test = new Settings(args[1],0);	
				System.out.println("************************************");
				System.out.println("Found new setting:");
				System.out.println("HDFS:            " + test.getHDFSPath());
				System.out.println("Local:           " + test.getLocalPath());
				System.out.println("Canopy T1:       " + test.getCanopyT1());
				System.out.println("Canopy T2:       " + test.getCanopyT2());
				System.out.println("Kmeans treshold: " + test.getKmeansTershold());
				System.out.println("Kmeans maxiter:  " + test.getKmeansMaxIter());
				System.out.println("Attack index:    " + test.getAttackTypeIndex());
				for (int j = 0; j < test.getFeaturesToSelectCount() ; j++) {
					System.out.println("Feature:         " + test.getFeaturesToSelect(j));
					System.out.println("Bins:            " + test.getBinCount(j));
				}
				System.out.println("Sign. lev.:       " + test.getSignificanceLevel());
			}
			System.exit(0);
		}
		
		if (args.length > 0 && args[0].equals("statdump")) {
			if (args.length == 1) {
				System.out.println("Specify filename!");
				System.exit(0);
			}
			try {
				DataInputStream dis = new DataInputStream(new FileInputStream(args[1]));
				ComputeStatistics.Statistics stat = new ComputeStatistics.Statistics();
				stat.readFields(dis);
				stat.dump(System.out);
				dis.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.exit(0);
		}
	}
	
	private static Settings currSettings = null;
	
	public static Settings getCurrentSettings() {
		return currSettings;
	}
	
	@SuppressWarnings("deprecation")
	private static void prepareFileSystems(String rawDataPath) throws IOException, URISyntaxException {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(new java.net.URI("hdfs:/"),conf);
		if (hdfs.exists(new Path(getCurrentSettings().getHDFSPath())))
			hdfs.delete(new Path(getCurrentSettings().getHDFSPath()));
		hdfs.mkdirs(new Path(getCurrentSettings().getHDFSPath()));
		hdfs.mkdirs(new Path(getCurrentSettings().getHDFSPath()+"/rawdata"));
		
		hdfs.copyFromLocalFile(new Path(rawDataPath), new Path(getCurrentSettings().getHDFSPath()+"/rawdata/rawdata.csv"));
	}
	
	private static void copyAllPartToLocalFs(String dir, String localNamePreffix) throws IOException, URISyntaxException {
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(new java.net.URI("hdfs:/"),conf);
		FileStatus [] fileStatus = hdfs.listStatus(new Path(dir));
		for (int i = 0; i < fileStatus.length ; i++)
			if (fileStatus[i].getPath().getName().toString().startsWith("part-"))
				hdfs.copyToLocalFile(fileStatus[i].getPath(), new Path(getCurrentSettings().getLocalPath()+"/"+localNamePreffix+"."+fileStatus[i].getPath().getName().toString()));
	}
	
	@SuppressWarnings("deprecation")
	private static void getResultsToLocal() throws IOException, URISyntaxException {
		File dir = new File(getCurrentSettings().getLocalPath());
		if (dir.exists())
			dir.delete();
		
		dir.mkdir();
		
		if (getCurrentSettings().getRunGrubbs()) {
			copyAllPartToLocalFs(getCurrentSettings().getHDFSPath()+grubbsTestResultPath, "grubbs.detailed.result");
			copyAllPartToLocalFs(getCurrentSettings().getHDFSPath()+grubbsTestResultSummaryPath, "grubbs.result");
		}
		if (getCurrentSettings().getRunHBOS()) {
			copyAllPartToLocalFs(getCurrentSettings().getHDFSPath()+HBOSDataPath, "HBOS.detailed.result");
			copyAllPartToLocalFs(getCurrentSettings().getHDFSPath()+HBOSSummaryPath, "HBOS.result");
		}
		if (getCurrentSettings().getRunMahout()) {
			copyAllPartToLocalFs(getCurrentSettings().getHDFSPath()+mahoutClusteringPath, "mahout.detailed.result");
			copyAllPartToLocalFs(getCurrentSettings().getHDFSPath()+mahoutClusteringSummaryPath, "mahout.result");
		}
		
		Configuration conf = new Configuration();
		FileSystem hdfs = FileSystem.get(new java.net.URI("hdfs:/"),conf);
		hdfs.delete(new Path(getCurrentSettings().getHDFSPath()));
	}
	
	private static void writeOutRunTimes(String msg) throws IOException {
		String path = getCurrentSettings().getLocalPath()+"/runtimes.txt";
		if (path.startsWith("file:"))
			path = path.substring(5);
		FileWriter fw = new FileWriter(path);
		try {
			fw.write(msg);
		} finally {
			fw.close();
		}
	}
	
	private static String formatTime(long millis) {
		if (millis > 2*60*60*1000)
			return millis/60/60/1000 + " hour";
		else if (millis > 2*60*1000)
			return millis /60/1000 + " minute";
		else if (millis > 2*1000)
			return millis/1000 + " second";
		else
			return millis + " ms";
	}
	
	private static void runSetting(String [] args, String rawDataPath) throws Exception {
		prepareFileSystems(rawDataPath);
		
		String runTimes = "";
		long time;
		
		time = System.currentTimeMillis(); 
		ToolRunner.run(new FilterInputJob(), new String[] {} );
		runTimes = runTimes + "Filtering input took " + formatTime(System.currentTimeMillis()-time) + ".\n";
		time = System.currentTimeMillis(); 
		ToolRunner.run(new MultivariateStatisticsJob(), new String[] {} );
		runTimes = runTimes + "Multivariate statistics took " + formatTime(System.currentTimeMillis()-time) + ".\n";
		
		if (getCurrentSettings().getRunGrubbs()) {
			time = System.currentTimeMillis(); 
		    ToolRunner.run(new ProjectedStatisticsJob(), new String[] {} );
		    runTimes = runTimes + "Projected statistics took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
		    ToolRunner.run(new ZScoreComputeJob(), new String[] {} );
		    runTimes = runTimes + "Z-score computing took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
		    ToolRunner.run(new ZScoreEvaluateJob(), new String[] {} );
		    runTimes = runTimes + "Z-score evaluating took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
			ToolRunner.run(new ZScoreSummarizeJob(),new String[] {} );
			runTimes = runTimes + "Z-score evaluating (summary) took " + formatTime(System.currentTimeMillis()-time) + ".\n";
		}
		if (getCurrentSettings().getRunHBOS() || getCurrentSettings().getRunMahout()) {
			if (getCurrentSettings().getNormalizationType() == Settings.NormalizationType.ZNORM) {
				time = System.currentTimeMillis(); 
			    ToolRunner.run(new ZNormalizeJob(), new String[] {} );
			    runTimes = runTimes + "Data normalization(z-norm) took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			} else {
				time = System.currentTimeMillis(); 
			    ToolRunner.run(new MinMaxNormalizeJob(), new String[] {} );
			    runTimes = runTimes + "Data normalization(minmax-norm) took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			}
		}
		if (getCurrentSettings().getRunMahout()) {
			time = System.currentTimeMillis(); 
			Configuration mahoutConf = new Configuration();
			Settings.saveCurrent(mahoutConf);
		    ToolRunner.run(mahoutConf, new MahoutExportJob(), args );
		    runTimes = runTimes + "Exporting to Mahout took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
			//mahoutConf.set("mapred.map.child.log.level", "DEBUG");
		    CanopyDriver.run(mahoutConf, new Path(getCurrentSettings().getHDFSPath()+mahoutInputPointsPath), new Path(getCurrentSettings().getHDFSPath()+canopyOutputPath), new EuclideanDistanceMeasure(), getCurrentSettings().getCanopyT1(), getCurrentSettings().getCanopyT2(), false, 0.0, false);
		    runTimes = runTimes + "Canopy clustering took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
			KMeansDriver.run(mahoutConf, new Path(getCurrentSettings().getHDFSPath()+mahoutInputPointsPath), new Path(getCurrentSettings().getHDFSPath()+kmeansInputClusters), new Path(getCurrentSettings().getHDFSPath()+kmeansOutputPath), new EuclideanDistanceMeasure(), getCurrentSettings().getKmeansTershold(), getCurrentSettings().getKmeansMaxIter(), true, 0.0, false);
		    runTimes = runTimes + "Kmeans clustering took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
			ToolRunner.run(mahoutConf, new MahoutEvaluateJob(), args );
		    runTimes = runTimes + "Evaluate Mahout results (classification) took " + formatTime(System.currentTimeMillis()-time) + ".\n";
		    time = System.currentTimeMillis(); 
			ToolRunner.run(mahoutConf, new MahoutSummarizeJob(), args );
		    runTimes = runTimes + "Summarize Mahout results (classification) took " + formatTime(System.currentTimeMillis()-time) + ".\n";
		
		}
		if (getCurrentSettings().getRunHBOS()) {
			time = System.currentTimeMillis(); 
		    ToolRunner.run(new BuildHistogramJob(), new String[] {} );
		    runTimes = runTimes + "Building histograms took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
		    ToolRunner.run(new HistrogramEvaluateJob(), new String[] {} );
		    runTimes = runTimes + "Calculate HBOS-s took " + formatTime(System.currentTimeMillis()-time) + ".\n";
			time = System.currentTimeMillis(); 
			ToolRunner.run(new HistrogramSummarizeJob(), new String[] {} );
		    runTimes = runTimes + "Calculate HBOS-s(summary) took " + formatTime(System.currentTimeMillis()-time) + ".\n";
		} 
	    
	    getResultsToLocal();
	    writeOutRunTimes(runTimes);
	}
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		Debug(args);		
		
		String xmlFile = null;
		String rawdataPath = null;
		for (int i = 0 ; i < args.length ; i++) {
			if (args[i].equals("--xmlconfig") && args.length > i+1)
				xmlFile = args[i+1];
			if (args[i].equals("--rawdata") && args.length > i+1)
				rawdataPath = args[i+1];
		}
		if (xmlFile == null)
			throw new RuntimeException("No XML file specified!");
		if (rawdataPath == null)
			throw new RuntimeException("No raw data csv file specified!");
		
		for (int i = 0 ; i < Settings.getConfigCount(xmlFile) ; i++) {
			System.out.println("Running detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + "...");
			System.err.println("Running detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + "...");
			currSettings = new Settings(xmlFile,i);
			try {
				runSetting(args,rawdataPath);
				System.out.println("Done running detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + ".");
				System.err.println("Done running detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + ".");
			} catch (Exception e) {
				try {
					e.printStackTrace();
					System.out.println("Failed to run detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + ", try to rerun it...");
					System.err.println("Failed to run detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + ", try to rerun it...");
					runSetting(args,rawdataPath);
					System.out.println("Succesfully rerunned detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + "!");
					System.err.println("Succesfully rerunned detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + "!");
				} catch (Exception ex) {
					ex.printStackTrace();
					System.out.println("Failed to rerun detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + ", skipped it, try to rerun manually!");
					System.err.println("Failed to rerun detection " + (i+1) + " of " + Settings.getConfigCount(xmlFile) + ", skipped it, try to rerun manually!");
				}
			}
		}
	}

}
