package detection;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.Logger;
import org.apache.commons.math.linear.RealVector;

public class Normalize {
	public abstract static class MapNormalize extends Mapper<LongWritable, FilterInput.Record, LongWritable, FilterInput.Record> {
		protected ComputeStatistics.Statistics statistics;
		
		abstract protected void configure();
		
		@Override 
		protected void setup(Context context) {
			Configuration conf = context.getConfiguration();
			statistics = ComputeStatistics.loadStatisticsFromCache(conf.get("statistics.multivariate.relative"),conf);
			
			configure();
		}
		
		@Override
		protected void map(LongWritable key, FilterInput.Record value, Context context) throws IOException, InterruptedException {
			value.setAsVector( normalize(value.asVector()) );
			context.write(key, value);
		}
		
		protected abstract RealVector normalize(RealVector vector);
	}

	public static class MapZ extends MapNormalize {
		/**
		 * Store average and deviation for optimization purposes.
		 */
		RealVector avg;
		RealVector dev;
		
		@Override 
		protected void configure() {
			avg = statistics.avg();
			dev = statistics.dev();
			for (int i = 0 ; i < dev.getDimension() ; i++)
				if (dev.getEntry(i) == 0) {
					Logger.getLogger(this.getClass()).warn("The deviation of the feature is zero!");
					dev.setEntry(i,1.0);
				}
		}
		
		@Override
		protected RealVector normalize(RealVector vector) {
			RealVector v = vector.subtract(avg);
			for (int i = 0; i < v.getDimension() ; i++)
				v.setEntry(i, v.getEntry(i) / dev.getEntry(i) );
			return v;
		}

	}
	
	public static class MapMinMax extends MapNormalize {
		/**
		 * Store minimum and deviation for optimization purposes.
		 */
		RealVector min;
		RealVector diff;
		
		@Override 
		protected void configure() {
			min = statistics.min();
			diff = statistics.max().subtract(min);
			for (int i = 0 ; i < diff.getDimension() ; i++)
				if (diff.getEntry(i) == 0) {
					Logger.getLogger(this.getClass()).warn("The deviation of the feature is zero!");
					diff.setEntry(i,1.0);
				}
		}
		
		@Override
		protected RealVector normalize(RealVector vector) {
			RealVector v = vector.subtract(min);
			for (int i = 0; i < v.getDimension() ; i++)
				v.setEntry(i, v.getEntry(i) / diff.getEntry(i) );
			return v;
		}
		
	}
}
