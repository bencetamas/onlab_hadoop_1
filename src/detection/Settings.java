package detection;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Settings {
	private String hdfsPath;
	private String localPath;
	
	private double canopyT1;
	private double canopyT2;
	private double kmeansTershold;
	private int kmeansMaxIter;
	private double kmeansPdf = 0.0;
	
	private int attackTypeIndex = 41;
	private int [] featuresToSelect;
	private int [] binCounts;
	private int defaultBinCount = 10;
	
	private double significanceLevel = 0.95;
	private double HBOSBoundary = 1.0;
	
	public enum NormalizationType { MINMAX, ZNORM };
	private NormalizationType normalizationType = NormalizationType.MINMAX;
	
	private boolean runGrubbs = true;
	private boolean runHBOS = true;
	private boolean runMahout = true;
	
	public String getHDFSPath() { return hdfsPath; }
	public String getLocalPath() { return localPath; }
	
	public double getCanopyT1() { return canopyT1; }
	public double getCanopyT2() { return canopyT2; }
	public double getKmeansTershold() { return kmeansTershold; }
	public int getKmeansMaxIter() { return kmeansMaxIter; }
	public double getKmeansPdf() { return kmeansPdf; }
	
	public int getAttackTypeIndex() { return attackTypeIndex; }
	public int getFeaturesToSelectCount() { return featuresToSelect.length; }
	public int getFeaturesToSelect(int index) { return featuresToSelect[index]; }
	public int getBinCount(int index) { return binCounts[index]; }
	
	public double getSignificanceLevel() { return significanceLevel; }
	public double getHBOSBoundary() { return HBOSBoundary; }
	public NormalizationType getNormalizationType() { return normalizationType; }
	
	public boolean getRunGrubbs() { return runGrubbs; }
	public boolean getRunHBOS() { return runHBOS; }
	public boolean getRunMahout() { return runMahout; }
	
	public static int getConfigCount(String xmlFile) {
		File file = new File(xmlFile);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(file);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		doc.getDocumentElement().normalize();
		
		return doc.getElementsByTagName("config").getLength();
	}
	
	/**
	 * Returns he current setting of the JobController(!).
	 * @return the current setting of the JobController.
	 */
	public static Settings getCurrent() {
		return JobController.getCurrentSettings();
	}
	
	public static void saveCurrent(Configuration conf) {
		conf.set("settings.hdfspath", getCurrent().hdfsPath);
		conf.set("settings.localpath", getCurrent().localPath);
		conf.setDouble("settings.canopy.t1", getCurrent().canopyT1);
		conf.setDouble("settings.canopy.t2", getCurrent().canopyT2);
		conf.setDouble("settings.kmeans.treshold", getCurrent().kmeansTershold);
		conf.setInt("settings.kmeans.maxiter", getCurrent().kmeansMaxIter);
		conf.setDouble("settings.kmeans.pdf", getCurrent().kmeansPdf);
		conf.setInt("settings.attacktype.index", getCurrent().attackTypeIndex);
		conf.setInt("settings.features.count", getCurrent().featuresToSelect.length);
		conf.setInt("settings.features.default.bincount", getCurrent().defaultBinCount);
		for (int i = 0 ; i < getCurrent().featuresToSelect.length ; i++) {
			conf.setInt("settings.features."+i+".index", getCurrent().featuresToSelect[i]);
			conf.setInt("settings.features."+i+".bincount", getCurrent().binCounts[i]);
		}
		conf.setDouble("settings.significancelevel", getCurrent().significanceLevel);
		conf.setDouble("settings.hbosboundary", getCurrent().HBOSBoundary);
		conf.setEnum("settings.normalization", getCurrent().normalizationType);
		conf.setBoolean("settings.run.grubbs", getCurrent().runGrubbs);
		conf.setBoolean("settings.run.HBOS", getCurrent().runHBOS);
		conf.setBoolean("settings.run.mahout", getCurrent().runMahout);
	}
	
	public Settings(Configuration conf) {
		hdfsPath = conf.get("settings.hdfspath");
		localPath = conf.get("settings.localpath");
		canopyT1 = conf.getDouble("settings.canopy.t1",0.0);
		canopyT2 = conf.getDouble("settings.canopy.t2",0.0);
		kmeansTershold = conf.getDouble("settings.kmeans.treshold",0.0);
		kmeansMaxIter = conf.getInt("settings.kmeans.maxiter",0);
		kmeansPdf = conf.getDouble("settings.kmeans.pdf", 0.0);
		attackTypeIndex = conf.getInt("settings.attacktype.index",0);
		int count = conf.getInt("settings.features.count",0);
		defaultBinCount = conf.getInt("settings.features.default.bincount", 10);
		featuresToSelect = new int[count];
		binCounts = new int[count];
		for (int i = 0 ; i < count ; i++) {
			featuresToSelect[i] = conf.getInt("settings.features."+i+".index",0);
			binCounts[i] = conf.getInt("settings.features."+i+".bincount",0);
		}
		significanceLevel = conf.getDouble("settings.significancelevel", 0.0);
		HBOSBoundary = conf.getDouble("settings.hbosboundary",1.0);
		normalizationType = conf.getEnum("settings.normalization", NormalizationType.MINMAX);
		runGrubbs = conf.getBoolean("settings.run.grubbs", true);
		runHBOS = conf.getBoolean("settings.run.HBOS", true);
		runMahout = conf.getBoolean("settings.run.mahout", true);
	}
	
	public Settings(String xmlFile, int configIndex) {
		File file = new File(xmlFile);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		Document doc;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(file);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		doc.getDocumentElement().normalize();
		
		if (!doc.getDocumentElement().getNodeName().equals("configs"))
			throw new RuntimeException("Wrong XML document: root element must be 'configs'");
		
		Element selectedConfig = (Element) doc.getElementsByTagName("config").item(configIndex);
		
		hdfsPath = selectedConfig.getElementsByTagName("hdfspath").item(0).getChildNodes().item(0).getNodeValue();
		localPath = selectedConfig.getElementsByTagName("localpath").item(0).getChildNodes().item(0).getNodeValue();
		
		canopyT1 = Double.parseDouble(selectedConfig.getElementsByTagName("canopyt1").item(0).getChildNodes().item(0).getNodeValue());
		canopyT2 = Double.parseDouble(selectedConfig.getElementsByTagName("canopyt2").item(0).getChildNodes().item(0).getNodeValue());
		kmeansTershold = Double.parseDouble(selectedConfig.getElementsByTagName("kmeanstreshold").item(0).getChildNodes().item(0).getNodeValue());
		kmeansMaxIter = Integer.parseInt(selectedConfig.getElementsByTagName("kmeansmaxiter").item(0).getChildNodes().item(0).getNodeValue());
		if (selectedConfig.getElementsByTagName("kmeanspdf").getLength() > 0)
			kmeansPdf = Double.parseDouble(selectedConfig.getElementsByTagName("kmeanspdf").item(0).getChildNodes().item(0).getNodeValue());
		
		
		if (selectedConfig.getElementsByTagName("attacktypeindex").getLength() > 0)
			attackTypeIndex = Integer.parseInt(selectedConfig.getElementsByTagName("attacktypeindex").item(0).getChildNodes().item(0).getNodeValue());
		NodeList features = ((Element) selectedConfig.getElementsByTagName("features").item(0)).getElementsByTagName("feature");
		featuresToSelect = new int [features.getLength()];
		binCounts = new int [features.getLength()];
		if (selectedConfig.getElementsByTagName("defaultbincount").getLength() > 0)
			defaultBinCount = Integer.parseInt(selectedConfig.getElementsByTagName("defaultbincount").item(0).getChildNodes().item(0).getNodeValue());
		
		for (int i = 0 ; i < features.getLength() ; i++) {
			Element feature = (Element) features.item(i);
			featuresToSelect[i] = Integer.parseInt(feature.getElementsByTagName("index").item(0).getChildNodes().item(0).getNodeValue());
			if (feature.getElementsByTagName("bincount").getLength() > 0)
				binCounts[i] = Integer.parseInt(feature.getElementsByTagName("bincount").item(0).getChildNodes().item(0).getNodeValue());
			else
				binCounts[i] = defaultBinCount;
		}
		
		if (selectedConfig.getElementsByTagName("significancelevel").getLength() > 0)
			significanceLevel = Double.parseDouble(selectedConfig.getElementsByTagName("significancelevel").item(0).getChildNodes().item(0).getNodeValue());
		HBOSBoundary = Double.parseDouble(selectedConfig.getElementsByTagName("hbosboundary").item(0).getChildNodes().item(0).getNodeValue());
		if (selectedConfig.getElementsByTagName("normalize").getLength() > 0) {
			if (selectedConfig.getElementsByTagName("normalize").item(0).getChildNodes().item(0).getNodeValue().toUpperCase().equals("ZNORM"))
				normalizationType = NormalizationType.ZNORM;
			else if (selectedConfig.getElementsByTagName("normalize").item(0).getChildNodes().item(0).getNodeValue().toUpperCase().equals("MINMAX"))
				normalizationType = NormalizationType.MINMAX;
			else
				Logger.getLogger(this.getClass()).warn("Wrong value (" + selectedConfig.getElementsByTagName("significancelevel").item(0).getChildNodes().item(0).getNodeValue() +
														") in <normalization> tag, will use MINMAX normalization.");
		}
		
		if (selectedConfig.getElementsByTagName("rungrubbs").getLength() > 0)
			runGrubbs = Boolean.parseBoolean(selectedConfig.getElementsByTagName("rungrubbs").item(0).getChildNodes().item(0).getNodeValue());
		if (selectedConfig.getElementsByTagName("runhbos").getLength() > 0)
			runHBOS = Boolean.parseBoolean(selectedConfig.getElementsByTagName("runhbos").item(0).getChildNodes().item(0).getNodeValue());
		if (selectedConfig.getElementsByTagName("runmahout").getLength() > 0)
			runMahout = Boolean.parseBoolean(selectedConfig.getElementsByTagName("runmahout").item(0).getChildNodes().item(0).getNodeValue());
	}
}
	