# Compiling
local_home="/home/cloudera"
path="/home/cloudera/workspace/active"
local_doc="/home/cloudera/anomaly"

rm -rf $path/bin/*
rm -f $path/active.jar
javac -cp /usr/lib/hadoop/client/*:/usr/lib/hadoop/*:/usr/lib/mahout/* -d $path/bin/ $(find $path/src/ -name *.java)
jar -cvf $path/active.jar -C $path/bin .

#Running program
hdfs_path="/user/cloudera/anomaly"

export HADOOP_CLASSPATH="/usr/lib/mahout/*"
hadoop jar $path/active.jar detection.JobController \
	testxmlconfig /home/cloudera/anomaly/test.xml ;
